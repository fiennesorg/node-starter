package org.fiennes.nodestarter;

import org.apache.tools.ant.Task;
import org.cord.node.config.NodeConfig;
import org.cord.node.config.NodeConfig.Namespace;
import org.cord.util.Gettable;
import org.eclipse.jetty.rewrite.handler.RewriteHandler;
import org.eclipse.jetty.rewrite.handler.RewriteRegexRule;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.util.RolloverFileOutputStream;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.eclipse.jetty.util.resource.FileResource;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;

public class Jetty extends Task
{
  private static final String PORT = "appserver.port";
  private static final String LIBPATH = "appserver.libpath";
  private static final String REWRITES = "appserver.rewrites";
  private static final String STATICS = "appserver.statics.dirs";
  private static final String CACHECONTROL = "appserver.statics.cacheControl";
  private static final String NO_OUTPUT_LOGGING = "appserver.noOutputLogging";
  
  private static final String OUTLOG = "log/out.yyyy_mm_dd.log";
  private static final String REQLOG = "log/req.yyyy_mm_dd.log";
  
  private static void log(String fmt, Object... args)
  {
    if (!fmt.endsWith("\n")) {
      fmt = fmt + "\n";
    }
    System.err.format(fmt, args);
  }
  
  private static void usage()
  {
    System.err.println("node-starter [NODE-PROPERTIES [NODE-PROPERTIES [...]]]]");
  }
  
  private static NodeConfig nodeConfig(String appDir, String[] args)
  {
    NodeConfig config = null;
    File docBase = new File(appDir, "www");

    if (args.length > 0) {
      File[] confFiles = new File[args.length];

      for (int i = 0; i != args.length; ++i) {
        confFiles[i] = new File(args[i]);
      }

      config = 
        new NodeConfig(docBase, 
                       new PrintWriter(new OutputStreamWriter(System.err),
                                       true), confFiles);
    }
    else {
      config = 
        new NodeConfig(docBase, 
                       new PrintWriter(new OutputStreamWriter(System.err),
                                       true));
    }

    return config;
  }

  private static List<URL> makeClassPath(String libPath) throws Exception
  {
    List<URL> result = new LinkedList<URL>();
    for (String path: libPath.split(":")) {
      result.addAll(expandJars(Resource.newResource(path)));
    }
    return result;
  }
  
  // =========================================================================
  // The code for expandJars and initClassLoader is based on the code from
  // Runner.java in jetty-runner in the jetty-contrib distribution

  private static List<URL> expandJars(Resource lib) throws Exception
  {
    List<URL> result = new LinkedList<URL>();

    String[] list = lib.list();
    if (list == null)
      return result;

    for (String path : list) {
      if (".".equals(path) || "..".equals(path))
        continue;

      Resource item = lib.addPath(path);

      if (item.isDirectory())
        result.addAll(expandJars(item));
      else {
        if (path.toLowerCase().endsWith(".jar")
          || path.toLowerCase().endsWith(".zip")) {
          URL url = item.getURL();
          result.add(url);
        }
      }
    }
    
    return result;
  }

  private static void initClassLoader(List<URL> classPath)
  {
    ClassLoader currentCL = Thread.currentThread().getContextClassLoader();
    ClassLoader newCL = null;

    if (currentCL == null)
      newCL =  new URLClassLoader(classPath.toArray(new URL[classPath.size()]));
    else
      newCL =  new URLClassLoader(classPath.toArray(new URL[classPath.size()]), currentCL);

    Thread.currentThread().setContextClassLoader(newCL);
  }
  
  // =========================================================================
  // Jetty's StdErrLog doesn't use printStackTrace: it should.  We fix that
  // here.
  // TODO: We really, really need to switch to using something like slf4j
  
  private static class StdErrLog implements Logger {

    StdErrLog(boolean debug)
    {
      _debug = debug;
    }

    // -----------------------------------------------------------------------
    // Logger implementation
    
    /* Debug */
    
    @Override public boolean isDebugEnabled() { return _debug; }
    @Override public void setDebugEnabled(boolean enabled) { _debug = enabled; }

    @Override
    public void debug(String msg) 
    { 
      if (_debug)
        format(Level.DEBUG, msg);
    }

    @Override
    public void debug(String msg, Throwable e)
    { 
      if (_debug)
        format(Level.DEBUG, msg, e);
    }
    
    @Override
    public void debug(String msg, Object o1, Object o2)
    { 
      if (_debug)
        format(Level.DEBUG, msg, o1, o2);
    }

    /* Warn */

    @Override public void warn(String msg) { format(Level.WARN, msg); }
    @Override public void warn(String msg, Throwable e) { format(Level.WARN, msg, e); }
    @Override public void warn(String msg, Object o1, Object o2) { format(Level.WARN, msg, o1, o2); }
    
    /* Info */

    @Override public void info(String msg) { format(Level.INFO, msg); }
    @Override public void info(String msg, Object o1, Object o2) { format(Level.INFO, msg, o1, o2); }

    /* Log hierarchy */

    /* Don't use a log hierarchy: we only want one logger, and this is it */
    @Override public StdErrLog getLogger(String name) { return this; }

    @Override public String getName() { return ""; }

    //------------------------------------------------------------------------
    // Implementation details
  
    private boolean _debug = false;
    
    private PrintStream _out = System.err;

    private enum Level {
      DEBUG, WARN, INFO;
    };
    
    /* The logging methods this implements does not appear to be documented,
     * but the implementations are all consistent: if msg contains '{}'
     * sequences, then these should be replaced with arg0 and arg1 in that
     * order. */
    private void format(Level level, String msg, Object arg0, Object arg1)
    {
      int i0 = msg.indexOf("{}");
      int i1 = i0 < 0 ? -1 : msg.indexOf("{}", i0 + 2);

      if (arg1 != null && i1 >= 0)
          msg = msg.substring(0,i1) + arg1 + msg.substring(i1 + 2);
      if (arg0 != null && i0 >= 0)
          msg = msg.substring(0,i0) + arg0 + msg.substring(i0 + 2);
      
      _out.print(level);
      _out.print(':');
      _out.println(msg);
    }

    private void format(Level level, String msg)
    {
      _out.print(level);
      _out.print(':');
      _out.println(msg);
    }

    private void format(Level level, String msg, Throwable e)
    {
      _out.print(level);
      _out.print(':');
      _out.print(msg);
      _out.println(':');
      e.printStackTrace(_out);
    }

  }

  private static void initLogging(Gettable nodeConfig)
  {
    String logConfigLevel = nodeConfig.getString("log.config.level");
    Log.setLog(new StdErrLog("debug".equals(logConfigLevel)));
  }

  // =========================================================================

  private static void logOutput(String appDir) throws Exception
  {
    // Redirect stderr/stdout to a rotating log file
    String outlog = (new File(appDir, OUTLOG)).getCanonicalPath();
    PrintStream out =
        new PrintStream(new RolloverFileOutputStream(outlog, true, 30));
    System.setErr(out);
    System.setOut(out);
  }
  
  private static void logRequests(String appDir, HandlerCollection handlers) throws Exception
  {
    // Use a rotating log file for the request log
    String reqlog = (new File(appDir, REQLOG)).getCanonicalPath();
    NCSARequestLog requestLog = new NCSARequestLog(reqlog);
    RequestLogHandler logHandler = new RequestLogHandler();
    logHandler.setRequestLog(requestLog);
    
    handlers.addHandler(logHandler);
  }
  
  private static void runServer(String appDir, String docBase,
                                String rewrites, String statics,
                                String cacheControl,
                                int port) throws Exception
  {
    FileResource.setCheckAliases(false);
    
    Server server = new Server();

    Connector connector = new SelectChannelConnector();
    connector.setPort(port);
    server.setConnectors(new Connector[] { connector });
    
    HandlerCollection handlers = new HandlerCollection();
    logRequests(appDir, handlers);
    server.setHandler(handlers);

    HandlerList listHandlers = new HandlerList();

    // The containment hierarchy of handlers is as follows:
    // LogHandler
    // RewriteHandler
    //   DefaultServlets (one for each statics directory)
    //   WebAppContext
    
    //------------------------------------------------------------------------
    // URL Rewriting
    
    RewriteHandler rewrite = new RewriteHandler();
    rewrite.setHandler(listHandlers);
    rewrite.setRewriteRequestURI(true);
    rewrite.setRewritePathInfo(true);
    rewrite.setOriginalPathAttribute("requestedPath");

    if (rewrites != null) {
      String[] rules = rewrites.split("\\s*;\\s*");
      for (String rule : rules) {
        String[] parts = rule.split("\\s*->\\s*");
        if (parts.length == 2 && !parts[0].isEmpty() && !parts[1].isEmpty()) {
          log("Adding rewrite handler: %s", rule);
          RewriteRegexRule rewriteRule = new RewriteRegexRule();
          rewriteRule.setRegex(parts[0]);
          rewriteRule.setReplacement(parts[1]);
          rewrite.addRule(rewriteRule);
        }
      }
    }

    handlers.addHandler(rewrite);
    
    //------------------------------------------------------------------------
    // Static content

    if (statics != null) {
      for (String path : statics.split("\\s*;\\s*")) {
        if (!path.isEmpty()) {
          log("Adding static dir: %s", path);
          ContextHandler context = new ContextHandler();
          context.setContextPath(path);
          context.setResourceBase(docBase + path);
          ResourceHandler resourceHandler = new ResourceHandler();
          if (cacheControl != null) {
            resourceHandler.setCacheControl(cacheControl);
          }
          context.setHandler(resourceHandler);
          context.setAliases(true);
          listHandlers.addHandler(context);
        }
      }
    }

    //------------------------------------------------------------------------
    // Servlet
    
    WebAppContext webapp = new WebAppContext();
    webapp.setResourceBase(Resource.newResource(docBase).toString());
    webapp.setAliases(true);
    listHandlers.addHandler(webapp);

    //------------------------------------------------------------------------
    // Go!

    server.start();
    while (System.in.available() == 0) {
      Thread.sleep(5000);
    }
    server.stop();
    server.join();
  }
  
  public static void main(String[] args) throws Exception
  {
    if (args.length > 0 && args[0].equals("--help")) {
      usage();
      return;
    }
    
    String appDir = System.getProperty("user.dir");
    NodeConfig config = nodeConfig(appDir, args);
    Gettable nodeConfig = config.getGettable(Namespace.NODE);

    String libPath = nodeConfig.getString(LIBPATH);
    if (libPath != null) {
      initClassLoader(makeClassPath(libPath));
    }

    String portStr = nodeConfig.getString(PORT);
    if (portStr == null) {
      log("No port found in config for %s", config.getHostName());
      System.exit(1);
    }
    int port = 0;
    try {
      port = Integer.parseInt(portStr);
    }
    catch(NumberFormatException ex) {
      log("Non-integer value ('%s') for port found in config for .%s",
          portStr, config.getHostName());
      System.exit(1);
    }

    initLogging(nodeConfig);
    
    String noOutputLogging = nodeConfig.getString(NO_OUTPUT_LOGGING);
    if (noOutputLogging == null)
      logOutput(appDir);
    
    runServer(appDir, config.getDocBase(),
              nodeConfig.getString(REWRITES),
              nodeConfig.getString(STATICS),
              nodeConfig.getString(CACHECONTROL), 
              port);
  }
}
